public class pesanan extends kue {
    private double berat;

    public pesanan(String name, double price, int stock) {
        super(name, price, stock);
    }

    //menghitung harga kue pesanan dengan mengambil dari method getPrice dari superclass
    //dan dikalikan dengan Berat yang diambil dari method Berat
    @Override
    public double hitungHarga() {
        return getPrice() * Berat();
    }

    //Berat dari kue pesanan adalah sama dengan stock pada class kue
    //jadi saya membuat berat pada kue pesanan,dimana
    //Berat = Stock 
    @Override
    public double Berat() {
         berat = getStock();
         return berat;
    }

    //karna method abstract pada kue class ,maka method ini harus diletakkan juga di sini,namun
    //pada kue pesanan ,"jumlah" tidak diperhitungkan
    //yang diperhitungkan adalah berat
    //jadi untuk kue pesanan,mereturn 0;
    @Override
    public double Jumlah() {
        return 0;
    }
}


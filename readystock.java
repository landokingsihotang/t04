public class readystock extends kue {
    private double jumlah;

    public readystock(String name, double price, int stock) {
        super(name, price, stock);

    }
    //menghitung harga kue jadi (readyStock) dengan mengambil dari method getPrice dari superclass
    //dan dikalikan dengan Jumlah yang diambil dari method Jumlah
    @Override
    public double hitungHarga() {
        return getPrice() * Jumlah() * 2;
    }

    //karna method abstract pada kue class ,maka method ini harus diletakkan juga di sini,namun
    //Method Berat tidak diperhitungkan pada class kue jadi (readyStock)
    //yang diperhitungkan adalah berat
    //sehingga method berat return 0;
    @Override
    public double Berat() {
        return 0;
    }

     //jumlah dari kue jadi adalah sama dengan stock pada class kue
    //jadi saya membuat jumlah pada kue jadi (readyStock),dimana
    //jumlah = Stock
    @Override
    public double Jumlah() {
        jumlah = getStock();
        return jumlah;
    }
}

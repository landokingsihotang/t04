public abstract class kue{
    private String name;
    private double price;
    private double stock;
    public kue(String name, double price, int stock){
        this.name = name;
        this.price = price;
        this.stock = stock;
    }
    //mengambil nama kue
    public String getName() {
        return name;
    }
    //method ini tidak digunakan pada program
    public void setName(String name) {
        this.name = name;
    }
    //mengambil harga kue
    public double getPrice() {
        return price;
    }
    //method ini tidak digunakan
    public void setPrice(double price) {
        this.price = price;
    }

    //method ini untuk menampilkan jumlah untuk kue pesanan
    //dan jumlah untuk kue jadi (readystock)
    public double getStock(){
        return this.stock;
    }
    abstract public double hitungHarga();
    abstract public double Berat();
    abstract public double Jumlah();
    @Override
    public String toString(){
        return String.format("---------- %s ----------\nHarga\t\t: %,.1f",getName(),getPrice());
    }
}
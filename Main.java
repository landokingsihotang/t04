//Main class yang menjalankan program
public class Main {
    public static void main(String[] args) {
     //Deklarasi array objek bertipe 'kue'
    kue cake[] = new kue[5];
     //Membuat lima objek yang berbeda untuk disimpan di dalam array
    cake[0] = new pesanan ("Brownis",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[1] = new pesanan("Bolu",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[2] = new pesanan("Kue Sus",(int)(((Math.random()*(10-1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[3] = new readystock("Kue Pisang",(int)(((Math.random()*(10 - 1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
    cake[4] = new readystock("Onde-Onde",(int)(((Math.random()*(10 - 1)) +1) *1000),(int) (((Math.random() * (20 - 1)) +1) *0.5));
     //Melakukan loop melalui array dan mencetak detail dari setiap objek
     System.out.println("\tDetail Kue Pesanan\n================================");
    for(int i = 0; i<3 ;i++){
        System.out.println(cake[i].toString());
        System.out.println("Stock\t\t: "+cake[i].getStock());
        System.out.printf("Harga Total\t: %,.1f",cake[i].getPrice()*cake[i].getStock());
        System.out.println();
    }
    System.out.println();
    System.out.println("\tDetail Kue Jadi\n================================");
    for(int i = 3; i<5 ;i++){
        System.out.println(cake[i].toString());
        System.out.println("Stock\t\t: "+cake[i].getStock());
        System.out.printf("Harga Total\t: %,.1f",cake[i].getPrice()*cake[i].getStock()*2);
        System.out.println();
    }
     //Menghitung total harga semua kue yang ada
    double totalSemua=0;
    for(kue jajan : cake){
        totalSemua += jajan.hitungHarga();
    }
     //Menghitung total harga dan total berat untuk kue yang dipesan
    double totalHargaPesanan=0;
    for(int i = 0; i < 3; i++){
        totalHargaPesanan += cake[i].hitungHarga();
    }
    double totalBeratPesanan=0;
    for(int i = 0; i < 3; i++){
        totalBeratPesanan += cake[i].Berat();
    }
     //Menghitung total harga dan jumlah untuk kue yang siap di jual
    double totalHargaJadi=0;
    for(int i = 3; i < 5; i++){
        totalHargaJadi += cake[i].hitungHarga();
    }
    double totalJumlahJadi=0;
    for(int i = 3; i < 5; i++){
        totalJumlahJadi += cake[i].Jumlah();
    }
     //Mencetak total yang dihitung
    System.out.println("----------------------------------");
    System.out.printf("Total Harga Kue\t\t:%,.1f\n",totalSemua);
    System.out.printf("Total Harga Kue Pesanan\t:%,.1f\n",totalHargaPesanan);
    System.out.printf("Total Berat Kue Pesanan\t:%,.1f\n",totalBeratPesanan);
    System.out.printf("Total Harga Kue Jadi\t:%,.1f\n",totalHargaJadi);
    System.out.printf("Total Jumlah Kue Jadi\t:%,.1f\n",totalJumlahJadi);
     //Melakukan loop melalui array, menemukan kue dengan harga tertinggi, dan mencetak detail kue tersebut
     //kue yang dimaksud disini adalah total harga kue ,
     //bukan harga sebuah kue 
    double maks = 0;
    for(int i = 0; i < 5; i++){
        maks = Math.max(maks, cake[i].hitungHarga());
    }
    for(int i = 0; i < 5; i++){
        if (maks == cake[i].hitungHarga()){
            System.out.println("Kue Termahal\t\t:"+cake[i].getName());
        }
    }
    }
}